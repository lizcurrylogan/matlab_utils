function wrt_projected_age_data(age_obs,age_unc,file_prefix,flag_val);

%% This is a function designed to project the gridded age layer data set from
%  NASA OIB to a grid amenable for assimilation into Sicopolis-AD. It
%  is a work in progress.
%  Last edited: Liz C. Logan (Jun292018)
%%

cd /Users/Logan/SicoAD_local/SICOoutput/Nature

if (ischar(file_prefix))
    file_str_unc = [file_prefix '_unc.dat'];
    file_str_obs = [file_prefix '_obs.dat'];
    file_str_meta= [file_prefix '_meta.txt'];
else
    error_msg = ['3rd argument passed to function isn"t a string;' ...
                 ' please supply a string! that requires use of' ...
                 ' SINGLE QUOTES around your intended file name'];
    error(error_msg);
end

% scrub the data so it is digestible to SicopolisAD:
age_obs( isnan(age_obs)==1 ) = flag_val;
age_unc( isnan(age_unc)==1 ) = flag_val;


% note: the passed data arguments must be ordered from [1...end]
% corresponding to [base ... surface] of ice sheet to work in the age cost
% function in SICOPOLIS-AD:

% also: the way these files are written is specific to SICOPOLIS: e.g., the
% use of white space (' ') delimiters below; if you change this, you must 
% also change subroutine read_ad_data() in 
% src/subroutines/openad/ctrl_m.F90 
% to reflect the formatted IO reading.
dimensions = size(age_obs);
zlevels = dimensions(3);
jmax    = dimensions(1);
imax    = dimensions(2);

% write the files:
fid_obs=fopen(file_str_obs,'w');
fid_unc=fopen(file_str_unc,'w');

% string pad test:
%ageot = floor(age_obs); ageut = floor(age_unc);
%ageot = str2double(pad(string(age_obs), 7, 'right', '0'));
%ageut = str2double(pad(string(age_unc), 7, 'right', '0'));

format shortE;

for k = 1:zlevels
    for j = 1:jmax
        for i = 1:imax

            
            %fwrite(fid_obs, evalc('disp(age_obs(j,i,k)) '));
            %fwrite(fid_unc, evalc('disp(age_unc(j,i,k)) '));

fprintf(fid_obs,'%11.4e',age_obs(j,i,k));
fprintf(fid_unc,'%11.4e',age_unc(j,i,k));
 
%fprintf(fid_obs,'%08.1f %c',age_obs(j,i,k), ' ');
%fprintf(fid_unc,'%08.1f %c',age_unc(j,i,k), ' ');

%fprintf(fid_obs,'%e %s',age_obs(j,i,k), ' ');
%fprintf(fid_unc,'%e %s',age_unc(j,i,k), ' ');
            
% deprecated: does not format properly to sicopolis
%dlmwrite(file_str_obs, age_obs(:,:,k), 'delimiter', ' ', '-append');
%dlmwrite(file_str_unc, age_unc(:,:,k), 'delimiter', ' ', '-append');
        end
        fprintf(fid_obs,'\n');
        fprintf(fid_unc,'\n');
    end
end

fclose(fid_obs);
fclose(fid_unc);

% % write a meta file about the data creation:
% meta_data = ['These ' file_prefix ' files were created on ' date '.\n'...
%              'Flag value for NaNs :=  %e12.4 .'];
% fid=fopen(file_str_meta, flag_val, 'w+');
% fprintf(fid,meta_data);
% fclose(fid);

dir=pwd;
disp(['Showing your newly created file ' file_str_obs ' in:'])
disp(dir)
disp('________________________________')
ls -lt
disp('________________________________')
disp('Now go send these over to your sicopolis working dir, and have fun.');


