%function project_age_data(plotting,simple_physics,raw)

%% This function was created to automate the down-sampling and projection 
%    of age-layer data from Greenland (https://nsidc.org/data/rrrag4) and 
%    ages produced by the forward simulation of ice sheet model SICOPOLIS
%    (www.sicopolis.net). 
%    SYNTACTICAL EXAMPLE:
%    matchmesh(PLOTTING, SIMPLE_PHYSICS, RAW) where
%       [PLOTTING, SIMPLE_PHYSICS, RAW] are LOGICAL
%       PLOTTING        plots a deprecated comparison of obs-mod
%       SIMPLE_PHYSICS  uses different simulations (you'll likely want
%       true)
%       RAW             (for the deprecated plotting tool) plots age
%       differences raw or norm'd by NASA's estimated uncertainty in ages
%    This function is a work in progress. 
%    Last edited: Liz C. Logan (Jun282018)
%%

% DEBUG ONLY (comment out function line):
if (1); clear; clf;
        plotting=false; simple_physics=true; raw=true; 
end;

% Do you want to plot?:
if (islogical(plotting)); 
    if (plotting); figno = 1; figure(figno); clf; end;
else
    error('First argument plotting needs to be true/false.')
end

% Do you want to view simple thermal physics or complex?
if (~islogical(simple_physics));
    error('Second argument simple_physics needs to be true/false.')
end

% Pick to plot raw ages or norm'd by uncertainty:
if (islogical(raw)); 
    if (raw); mu = 1e3; % raw ages for <obs - data>
    else;     mu = 1;   % norm'd by uncertainty
    end; 
else
    error('Third argument raw_or_normd needs to be true/false.')
end
% 

%% LOADING OBS AND MOD DATA:
% load Observational data:
cd /Users/Logan/Documents/Data/GreenlandAges; 
XOBS         = ncread('Greenland_age_grid.nc','x');
YOBS         = ncread('Greenland_age_grid.nc','y');
age_obs_full = ncread('Greenland_age_grid.nc','age_norm');
age_unc_full = ncread('Greenland_age_grid.nc','age_norm_uncert');


% load Model data:
if (simple_physics)
    cd /Users/Logan/SicoAD_local/SICOoutput/Nature/grl20000_Jun29_new_ref_output_20km/;
    filstr='new_ref_output_20km_forwardOG0011.nc';
else
    cd /Users/Logan/SicoAD_local/SICOoutput/Nature/CALCMOD3_SEALEVEL3_tweak/;
    filstr='v5_grl20_b2_paleo210007.nc';
end

% Reading in the Sico ages and grid:
XSICO = ncread(filstr,'x');
YSICO = ncread(filstr,'y');
age_mod  = ncread(filstr,'age_c');


%% Setting up grids for nearest point search:
% these will be the closest indices in X-Y space to the age-layer's grid:
iSico=[]; jSico=[]; 

% OBSERVATIONAL GRID:
% Clipping the grid so the nearest-neighbor search below works:
x_obs = XOBS(1300,:);
y_obs = YOBS(:,700);

% MODEL GRID: Sico grid needs to be put in kilmeters:
x_sico = XSICO/1000;
y_sico = YSICO/1000;

%% Doing the closest point search:
% X : filling in the iSico's here
for i=1:length(x_sico);
    
    % calculate the distance btwn current sico point and all obs points:
    dist = x_sico(i) - x_obs;
    
    % searches for nearest point (this is the crux):
    dummy = find(dist == min(abs(dist)) | dist == -min(abs(dist))); 
    
    % selects the first (floor) if finds more than one closest point:
    if(length(dummy) > 1); dummy = dummy(1); end;
    
    % now we append that (dummy) index to our running tally of closest
    % indices:
    iSico(end+1) = dummy;
end

% Y : filling in the jSico's here
for i=1:length(y_sico);
    
    % calculate the distance btwn current sico point and all obs points:
    dist = y_sico(i) - y_obs;
    
    % searches for nearest point (this is the crux):
    dummy = find(dist == min(abs(dist)) | dist == -min(abs(dist)));
    
    % selects the first (floor) if finds more than one closest point:
    if(length(dummy) > 1); dummy = dummy(1); end;

    % now we append that (dummy) index to our running tally of closest
    % indices:
    jSico(end+1) = dummy;
end
    
%% DOWNSAMPLING AGE-LAYER DATA TO SICOPOLIS GRID:

age_obs = age_obs_full(jSico,iSico,:);
age_unc = age_unc_full(jSico,iSico,:);

% re-arrange columns and flip the z axis:
age_mod = permute(age_mod,[2 1 3]);

% downsampling age_mod if max z-sigma (model) =/= z-sigma (observations)
if (~simple_physics);
    siz_mod = size(age_mod); z_mod_max = siz_mod(3); 
    siz_obs = size(age_obs); z_obs_max = siz_obs(3); 
    
    z_mod_levels = floor( linspace(1, z_mod_max, z_obs_max) );
    
    age_mod = age_mod(:,:,z_mod_levels);
end

% re-order the observations to match sicopolis z-sigma direction: 
age_obs = age_obs(:,:,[end:-1:1]);

% decide, finally, what agediff product we want to see:
if (mu > 1)
    agediff=(age_mod-age_obs);
else
    agediff=(age_mod-age_obs)./age_unc;
end

%%
% If plotting the point cloud:
if (plotting); ages_mod_obs_plot(agediff,figno,simple_physics,mu); end;

% IMPORTANT
% this function call writes the projected / downsampled NASA data to file
% with some meta data as well. NaNs are set to the flag value passed in the
% last argument:

file_prefix='gridded_age_spaces';
flag_val=-666; % it's important that this has a field width of 7 for
                  % file writing below

wrt_projected_age_data(age_obs,age_unc,file_prefix,flag_val);























