%cd /Users/Logan/Documents/Data/GreenlandAges
%age=ncread('Greenland_age_grid.nc','age_norm');
clf;
prompt='What depth of ice do you want to see? [1 25]';
zlevel=input(prompt);

viewing=true;

while(viewing)

    see_slice(zlevel,age); hold on;

    prompt=['Want to see more on top of that? [1 25]', ...
            '(Dont select the one you picked before,' ...
            'and type exactly either: y/n )'];
    still_looking=input(prompt,'s');
 
    if (still_looking=='y')
        prompt='What depth of ice do you want to see? [1 25]';
        zlevel=input(prompt);
        see_slice(zlevel,age); hold on;
        
    elseif (still_looking=='n')
        viewing=false;
    else
       disp(['Really, you gotta type either just a '...
             'lower case y or n and hit enter.',...
             ' I am not that slick with user-prompting code']); 
    end
    
    colorbar;
end;


% function see_slice(zlevel,age)
% 
% figure(1); view(-9,0); 
% contour3(age(:,:,zlevel),10); set(gca,'zdir','reverse');
% 
% figure(2);
% contour(age(),)
% 
% end