function ages_mod_obs_plot(agediff,figno,simple_physics,mu);

%% Make a point cloud-like plot of the metrics calculated by 
%  function project_age_data().
%  SYNTACTICAL eg:
%  ages_mod_obs_plot(agediff,FIGNO)
%   AGEDIFF is a 3D float array calculated by project_age_data
%   FIGNO   is an int figure number for the plot (also from same function).
%   SIMPLE_PHYSICS is a char str that will be used in plot titles 
%   mu      is a scalar float that gets used in norm'd versus raw agediff
%           comparisons
%  Last edited: Liz C. Logan (Jun292018)
%%

figure(figno);

% making the belly of the beast:
[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-15.000  ));
plot3(c,r,v,'k.'); hold on;

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-10.000 & agediff > mu*-15.000 ));
plot3(c,r,v,'ko');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-5.000 & agediff > mu*-10.000 ));
plot3(c,r,v,'kx'); 

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-3.000 & agediff > mu*-4.000 ));
plot3(c,r,v,'b.');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-2.500 & agediff > mu*-3.000 ));
plot3(c,r,v,'bo');

[r,c,v]=ind2sub(size(agediff),find(agediff <=-2.000 & agediff > mu*-2.500 ));
plot3(c,r,v,'bx');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-1.500 & agediff > mu*-2.000 ));
plot3(c,r,v,'c.');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-1.000 & agediff > mu*-1.500 ));
plot3(c,r,v,'co');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-.750 & agediff > mu*-1.000 ));
plot3(c,r,v,'cx'); 

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-.500 & agediff > mu*-.750 ));
plot3(c,r,v,'c<');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-.250 & agediff > mu*-.500 ));
plot3(c,r,v,'g.');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*-.100 & agediff > mu*-.250 ));
plot3(c,r,v,'go');

[r,c,v]=ind2sub(size(agediff),find(agediff <= mu*0 & agediff > mu*-.100 ));
plot3(c,r,v,'gx');

% 0 ---

[r,c,v]=ind2sub(size(agediff),find(agediff > mu*0 & agediff < mu*.100 ));
plot3(c,r,v,'yx');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*.100 & agediff < mu*.250 ));
plot3(c,r,v,'yo'); 

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*.250 & agediff < mu*.500 ));
plot3(c,r,v,'y.');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*.500 & agediff < mu*.750 ));
plot3(c,r,v,'y<'); 

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*.750 & agediff < mu*1.000 ));
plot3(c,r,v,'rx');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*1.000 & agediff < mu*1.500 ));
plot3(c,r,v,'ro');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*1.500 & agediff < mu*2.000 ));
plot3(c,r,v,'r.');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*2.000 & agediff < mu*2.500 ));
plot3(c,r,v,'r<');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*2.500 & agediff < mu*3.000 ));
plot3(c,r,v,'mx');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*3.000 & agediff < mu*4.000 ));
plot3(c,r,v,'mo');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*4.000 & agediff < mu*5.000 ));
plot3(c,r,v,'m<');      

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*5.000 & agediff < mu*10.000 ));
plot3(c,r,v,'m.');  

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*10.000 & agediff < mu*15.000 ));
plot3(c,r,v,'m.');

[r,c,v]=ind2sub(size(agediff),find(agediff >= mu*15.000  ));
plot3(c,r,v,'m.'); 

% PLOT ATTRIBUTES:
% legend and title for figure:

if (simple_physics)
    physics_type = ['SIMPLE THERMAL PHYSICS'];
else
    physics_type = ['COMPLEX THERMAL PHYSICS'];
end

if (mu == 1) % then we are looking at norm'd differences:
    diff_type = ['NORMALIZED '];
    expression= ['age_m_o_d - age_o_b_s / age_u_n_c'];
else
    diff_type = ['RAW '];
    expression= ['age_m_o_d - age_o_b_s'];
end

titstr=[{[diff_type 'AGE DIFFERENCE: ' expression ], physics_type}];

legstr=[{'(<-15]'},{'(-15--10]'},{'(-10--5]'},{'(-5--4]'},{'(-4--3]'},...
    {'(-3--2.5]'},{'(-2.5--2]'},{'(-2--1.5]'},{'(-1.5--1]'},...
    {'(-1--.75]'},{'(-.75--.5]'},{'(-.5--.25]'},{'(-.25--.1]'},{'(-.1-0]'},...
    {'(0-.1)'},{'[.1-.25)'},{'[.25-.5)'},{'[.5-.75)'},{'[.75-1)'},...
    {'[1-1.5)'},{'[1.5-2)'},{'[2-2.5)'},{'[2.5-3)'},{'[3-4)'},...
    {'[4-5)'},{'[5-10)'},{'[10-15)'},{'(>15]'}];

title(titstr)
legend(legstr)
